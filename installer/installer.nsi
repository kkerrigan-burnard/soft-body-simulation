!include "nsDialogs.nsh"
!include "LogicLib.nsh"
!include "zipdll.nsh"

# Define all the variables for the installer here

#default install path
!define installPath C:\cnd\KerriganKristian\videogame
!define basePath "C:\cnd\KerriganKristian\" 

#settings
!define installerName "Collision Physics Demo"
!define exeName "CollisionPhysicsDemoSetup.exe"
!define licenseFile "license.txt"

#zip file locations for zipDLL
!define baseGamePath "project02.zip"
!define extraAssetsZip "project02_extra.zip"
!define extraAssetsFolder "extra_assets\"
!define tempArchive "files.zip" #this is used to extract from a zip file during installation

#uninstaller
!define uninstallerName "uninstall.exe"
!define uninstallerShortName "Uninstall -Collision Physics Demo"

#shortcut
!define executable "KrisBryGame.exe"
!define shortcutName "Collision Physics Demo"

#registry information
!define root "Software\INFO6025\"
!define keyLocation "KerriganKristian"
!define keyName "videogame"

#Start of installation
Name "${installerName}"
OutFile "${exeName}"
LicenseData ${licenseFile}
XPStyle on

Var Dialog
Var Label
Var HeaderLbl
Var NextLbl
Var ExtraAssetsChbx
Var ExtraAssetsChbxState

Page custom nsWelcomePage nsWelcomePageLeave 
Page license nsLicensePage nsLicensePageLeave
Page custom nsExtraAssetsPage nsExtraAssetsPageLeave
Page instfiles

#Welcome Page!
Function nsWelcomePage
    nsDialogs::Create 1018
    Pop $Dialog

    ${if} $Dialog == error
    Abort
    ${EndIf}

    #Set the 'Next' button text
    GetDlgItem $0 $hwdparent 1;
    SendMessage $0 ${WM_SETTEXT} 0 `STR:$(^NextBtn)`;

    # Create labels
    ${NSD_CreateLabel} 0 0 100% 25u "Welcome to Kristian Kerrigan's Collision Detection Physics Demo"
    Pop $HeaderLbl

    ${NSD_CreateLabel} 0 50 100% 40u "Follow these easy steps and you will be on your way to testing out one of my many GDP school projects"
    Pop $Label

    ${NSD_CreateLabel} 0 80 100% 40u "The demo will be installed on your machine at ${installPath}"
    Pop $Label

    ${NSD_CreateLabel} 0 120 100% 15u "Finally, see the Controls.txt file that is installed on how to play!"
    Pop $Label

    ${NSD_CreateLabel} 150 170 100% 20u "Press Next to Continue"
    Pop $NextLbl

    #Set the font
    SendMessage $HeaderLbl ${WM_SETFONT} $1 1
    nsDialogs::Show

FunctionEnd

Function nsWelcomePageLeave

    #The next button will proceed to the next page

FunctionEnd

Function nsLicensePage

    #The license text is preloaded and will be displayed

FunctionEnd

Function nsLicensePageLeave

    #Proceed to the next page

FunctionEnd

#Extra Assets page
Function nsExtraAssetsPage

    nsDialogs::Create 
    Pop $Dialog

    ${if} $Dialog  == error
        Abort
    ${EndIf}

    ${NSD_CreateLabel} 0 0 100% 20u "Install new beta content?"
    Pop $HeaderLbl
    SendMessage $HeaderLbl ${WM_SETFONT} $1 1

    ${NSD_CreateLabel} 0 40 100% 30u "This is for demo purposes and will install some awesome Kingdom Hearts images with the game."
    Pop $Label

    #add the checkbox and uncheck by default
    ${NSD_CreateCheckbox} 0 100 100% 12u "&Install beta images."
	Pop $ExtraAssetsChbx
    ${NSD_SetState} $ExtraAssetsChbx "0"

    nsDialogs::Show

FunctionEnd

# See if the extra assets checkbox is checked on PageLeave
Function nsExtraAssetsPageLeave

    ${NSD_GetState} $ExtraAssetsChbx $ExtraAssetsChbxState

FunctionEnd

# Set up different font sizes
Section "Fonts"

    CreateFont $1 "Arial" "12" "500"
    CreateFont $2 "Arial" "14" "500"

SectionEnd

Section "Begin Copying files" section_index_output
  
	SetOutPath ${installPath}
	
	#extract the zip to the install path and delete the zip
	File "${basePath}${baseGamePath}"

    #use a common name for all installs
    Rename "${installPath}\${baseGamePath}" "${installPath}\${tempArchive}"
    zipDLL::extractall "${installPath}\${tempArchive}" ${installPath} 

    #be carefull here since we are deleting files
    Delete "${installPath}\${tempArchive}"

    # install the extra assets if the checkbox is checked
    ${If} $ExtraAssetsChbxState == ${BST_CHECKED}

        File "${basePath}${extraAssetsZip}"

        Rename "${installPath}\${extraAssetsZip}" "${installPath}\${tempArchive}"
        ZipDLL::extractall "${installPath}\${tempArchive}" "${installPath}\${extraAssetsFolder}"

        Delete "${installPath}\${tempArchive}"

    ${EndIf}

   
    #registry key (note that 32-bit goes to the Computer\HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\) 
    #this writes the full path to the executable to the registry
	WriteRegStr HKLM ${root}${keyLocation} ${keyName} "${installPath}\${executable}"
	
    #create shortcut to program
    CreateShortCut "$SMPROGRAMS\${shortcutName}.lnk" "${installPath}\${executable}" "" "${installPath}\assets\sphere.ico" 0 

    #create uninstaller
	WriteUninstaller ${installPath}\${uninstallerName}

    #create shortcut to unistaller
    CreateShortCut "$SMPROGRAMS\${uninstallerShortName}.lnk" "${installPath}\${uninstallerName}" "" "${installPath}\assets\sphere.ico" 0 

SectionEnd

Section "Uninstall"
    #delete the unistaller first (not sure why but some tutorials said so)
	Delete ${installPath}\${uninstallerName}

    #delete the install directory Caution: could delete needed directory
	RMDir /r "C:\cnd\KerriganKristian"
	
	#delete the shortcut
    Delete "$SMPROGRAMS\${shortcutName}.lnk"

    #delete the uninstall shortcut 
    Delete "$SMPROGRAMS\${uninstallerShortName}.lnk"

    #delete the registry entry
    #using the same root in the registry
    DeleteRegKey HKLM "${root}"

SectionEnd