/**
 * @file PathingStructs.h
 * @author  Kristian Kerrigan <k_kerrigan3@fanshaweonline.ca>
 * @version 1.0
 *
 * @section DESCRIPTION
 *
 * This file contains various structs that are used for pathing.
 * This includes both path following and path findling.
 */

#ifndef _PathingStructs_HG_
#define _PathingStructs_HG_

#include <gameMath.h>
#include <vector>

struct sPathNode {
	sPathNode(glm::vec3 position) : position(position) {}
	glm::vec3 position;
};

struct sPath {
	std::vector<sPathNode> pathNodes;
};


#endif // !_PathingStructs_HG_

