/**
 * @file
 * @author  Kristian Kerrigan <k_kerrigan3@fanshaweonline.ca>
 * @version 1.0
 *
 * @section DESCRIPTION
 *
 * Implementation of the cBehaviour class.
 */

#include "cBehaviourComponent.h"

 std::vector<iBehaviour*>& cBehaviourComponent::getBehaviours() {
	
	return this->mBehaviours;
}

void cBehaviourComponent::update(float deltaTime) {

	//// TODO: AI Project: Idle if no behaviours
	//if (this->mBehaviours.empty()) {

	//}

	//// Grab the first behaviour and call update
	//iBehaviour* currBehaviour = this->mBehaviours.front();
	//currBehaviour->update(deltaTime);

	//// Check if its finished and remove if so
	//if (currBehaviour->isFinished()) {

	//	this->mBehaviours.pop();
	//	delete currBehaviour;
	//}

	return;
}
